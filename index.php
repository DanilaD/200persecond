<?

class run {
  
  // относительный путь к директории для работы с файлами
    private $path = ['save' => '/data/', 'export' => '/export/'];
  
    
  // таблица в БД
    private $localhost = 'localhost'; 
    private $db_user = 'root'; 
    private $db_password = '';
    private $db_name = '200persecond';
    
    
  // переменные для работы и таблицы
    private $table_log = 'log';
    private $game_time = 'game_time';
    private $game_user_id = 'user_id';
    private $game_device_id = 'device_id';
    private $game_device_type = 'device_type';
    private $enum = ['1' => 'ipad','2' => 'iphone'];
    
    
  // переменные для работы экспорта
    private $table_export = 'export';
    private $export_date = 'date';
    private $export_status = 'status';
    private $export_link = 'link';
    private $export_update = 'update';
    private $export_enum = ['1' => 'start', '2' => 'process create file', '3' => 'file created', '4' => 'process delete row', '5' => 'row deleted', '6' => 'complited', '7' => 'error'];
   

  // необходимые ключи для работы с данными
    private function NecessaryData(){
      
      return [
        
        // игровое время события в формате Unix time
          $this->game_time,
        
        // ID игрока в Facebook (integer)
          $this->game_user_id,        
        
        // платформа устройства string (iPad, iPhone)
          $this->game_device_type,
        
        //  ID устройства (string, 64 символа)
          $this->game_device_id  
        
      ];
      
    }
    
        
  // обработать данные и выбрать только необходимые
    private function getData($get) {
      
      echo'<pre>';
      print_R($get);
      echo'</pre>';
      

      foreach (self::NecessaryData() as $key => $value) {
        
        // выборка нужных параметров, экранирование, еслин отсутствует обязательный параметр, то выход
        (isset($get[$value]) ? $array[$value] = self::AddOneRow($get, $value) : self::displayMessage());

      }
      
      return isset($array) ? $array : self::displayMessage();
      
    }
    
    
  // формирование одной строки
    private function AddOneRow($get, $value){
      
      return ($value != $this->game_device_type) ? ($get[$value]) : self::checkDeviceId($get, $value);
      
    }
    
  // проверить наличие платформ
  private function checkDeviceId($get, $value) {
        
    return isset(array_flip($this->enum)[mb_strtolower($get[$value])]) ? array_flip($this->enum)[mb_strtolower($get[$value])] : ''; 

  }
  
    
  // подготовить строку для записи в файл
    private function createDataForFile($data) {
      
      // обязательное наличие всех параметров для записи в БД
      if (count(self::NecessaryData()) != count($data)) return self::displayMessage();
      
      // преобразование для тхт файла
      return implode(',', $data);
      
    }
  
    
  // имя файла
    private function NameFile() {
      // генерится каждую минуту
      return dirname(__FILE__) . $this->path['save'] . date("YmdHi").'.txt';
      
    }
    

  // записать данные в файл
    private function safeDataToFile($data) {

      if (!$fh = fopen(self::NameFile(), "a")) return self::displayMessage();
      
      if (fwrite($fh, $data . PHP_EOL) === FALSE) return self::displayMessage(); 

      fclose($fh);

      return TRUE;

    }
    
    
  // получить данные из файла
  /*  
      private function getDataToFile($file) {
      
      return (file_get_contents($file));

    }
   */
    
    
  // проверить размер файла
    private function fileCheckBytes($file) {
            
      // проверить это файл
        if (is_file($file) === FALSE) return FALSE;
      
      // проверить размер
        if (filesize($file) < 1) {
          self::fileDelete($file);
          return FALSE;
        }
      
      return $file;
      
    }
    
    
  // удалить файл
    private function fileDelete($file) {
    
      unlink($file);
    
    }
       

  // считать файл для записи в БД
    private function getFiletoTransferToMySQL() {

      // сканирование файлов из директории
        $array = scandir(dirname(__FILE__) . $this->path['save']);

      // извлекаем последний элемент массива (в него происходит запись значений)
        array_pop($array);

      // работа с файлами
        $result = self::WorkEachFileToSaveToMysql($array); 

    }
  

  // подготовка данных и запись в БД
    private function WorkEachFileToSaveToMysql($array) {
      // если нет файлов для работы, то выход
        if (count($array) < 0) return FALSE;

      // работа с каждым файлов
        foreach ($array as $key => $value) {

          // првоерить файл 
          $file = self::fileCheckBytes(dirname(__FILE__) . $this->path['save'] . $value);

          // если это файл отсутствует или он пустой, то переход к следующему
          if (empty($file)) {continue;}

          // получить данные из файла
          //$data = self::getDataToFile($file);

          // записать в MySQL
          self::PrepareDataToInsertIntoMySql($file);

        }
    }
    

  // подготовить запрос для записи в MySQL
    private function PrepareDataToInsertIntoMySql($file) {

      // подготвка запроса
      $sql = 'LOAD DATA LOCAL INFILE \''.$file.'\' INTO TABLE '.$this->table_log.' FIELDS TERMINATED BY \',\'';

      // выполнение запроса
      $res = self::quareSql($sql);
      
      // удаление файла
      ($res['error'] == 0) ? self::fileDelete($file) : $res;

    }
  
 
  // создать директорию
    private function createPath() {
      
      foreach ($this->path as $value) {

        $dir = dirname(__FILE__) . $value;

        if (!is_dir($dir)) {
          
          mkdir($dir);
          $res[] = ['status'=> 'create dir', 'message' => $dir];
        
        }else{
          
          $res[] = ['status'=> 'there is dir', 'message' => $dir];
        
        }
        
      }
      
      return $res;
      
    }
    
     
  // получить интервал времени для выборки в БД
    private function getDateForMySQL($data){
      
      $data = empty($data) ? time() : $data;

      $data = date( "Y-m-d", $data );

      $res['day'] = strtotime($data);

      $res['preview'] = strtotime("$data - 1 day");

      return $res;
    
    }
    
  // вывести сообщение
    private function displayResult($res) {
      echo'<pre>';
      print_R($res->fetch_array(MYSQLI_ASSOC));
      echo'</pre>';
      exit;

    }

  // проверить формат даты
    private function validateDate($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }


    
  // запуск процесса выгрузки данных
    private function requestToExport($date){
      
      // проверить формат даты 2017-08-01 и преобразовать в UNIX TIME
      $res = self::validateDate($date, 'Y-m-d');
      
      if ($res === FALSE) {print_r('wrong format date');exit;}
      
      // получить диапазон для выбора дат
      $data = self::getDateForMySQL(strtotime($date));
      
      // проверить статус, если небыло выборки, то можно запускать
      $res = self::checkStatusExport($data['preview']);
      
      if ($res->num_rows > 0) self::displayResult($res);
      
      // установить статус СТАРТ
      $param['date'] = $data['preview'];
      $param['link'] = '';
      $param['status'] = 1;
      self::setStatusExport($param);
      
      // проверить статус, если небыло выборки, то можно запускать
      $res = self::checkStatusExport($data['preview']);
      
      if ($res->num_rows > 0) self::displayResult($res);
      
    }
    
  // подготовить имя файла
    private function prepareNameFileExport($data) {
      return $this->path['export'] . $data . '.csv';
    }
    
    
  // выгрузка из SQL данных в файл
    private function prepareToExport(){
      
    // проверить статус, есть ли ожидающие задачи со статусом start
      
      $res = self::checkStatusExport('',1);
      
      if ($res->num_rows == 0) self::displayResult($res);
      
    // данные из БД
      $array = $res->fetch_array(MYSQLI_ASSOC);
      
    // получить диапазон для выбора дат
      $data = self::getDateForMySQL($array['date']);
      
    // файл для записи
      $file = self::prepareNameFileExport($data['preview']);
      
    // дата выборки
      $param['date'] = $data['preview'];

    // ссылка на файл
      $param['link'] = $file;   

    // указать статус
      $param['status'] = 2;

    // запись статуса
      self::setStatusExport($param);
      
    // условие для выборки
      $where = self::whereForDb($data);
    
    // запрос для выполнения
      $sql = 'SELECT * FROM `'.$this->table_log.'` WHERE '.$where.' INTO OUTFILE \''.dirname(__FILE__) . $file.'\' FIELDS TERMINATED BY \',\' OPTIONALLY ENCLOSED BY \'"\' LINES TERMINATED BY \';\'';
      
    // выполнить запрос
      $res = self::quareSql($sql);
      
    // если нет ошибок, то установить статус создания файла
      if(($res['error'] == 0)) {
        // установить статус готовности файла
        $param['status'] = 3;
        self::setStatusExport($param);
      }
      
    // проверить статус, если небыло выборки, то можно запускать
      $res = self::checkStatusExport($data['preview']);
      
      if ($res->num_rows > 0) self::displayResult($res);
      
      return $res;
      
    }
    
    
    private function whereForDb($data) {
      return '`'.$this->game_time.'` >= \''.$data['preview'].'\' AND `'.$this->game_time.'` < \''.$data['day'].'\'';
    }
    
    
    private function checkStatusExport($date = '', $status = ''){
      
      $where = ($date) ? '`'.$this->export_date.'` = \''.$date.'\'' : '`'.$this->export_status.'` = '.$status.'';
      
      $sql = 'SELECT
              `'.$this->export_date.'`,
              `'.$this->export_status.'`,
              `'.$this->export_link.'`,
              `'.$this->export_update.'`
              FROM 
                `'.$this->table_export.'`
              WHERE 
                '.$where.'
              ';
       
      $res = self::quareSql($sql);
      
      return ($res['sql']);
      
    } 
    
    
    private function deleteFromDb() {
      
    // проверить статус, есть ли ожидающие задачи со статусом start  
      $res = self::checkStatusExport('',3);
      
      if ($res->num_rows == 0) self::displayResult($res);
      
    // данные из БД
      $array = $res->fetch_array(MYSQLI_ASSOC);
      
    // получить диапазон для выбора дат
      $data = self::getDateForMySQL($array['date']);
            
    // записать статус о начале удаления
      $param['date'] = $data['preview'];
      $param['link'] = self::prepareNameFileExport($data['preview']); 
      $param['status'] = 4;
      self::setStatusExport($param);
      
    // получить условие
      $where = self::whereForDb($data);
      
    // удаление из БД
      $sql = 'DELETE FROM `'.$this->table_log.'` WHERE '.$where;
              
    // выполнение
      self::quareSql($sql);
      
    // записать статус о окончании удаления
      $param['date'] = $data['preview'];
      $param['link'] = self::prepareNameFileExport($data['preview']); 
      $param['status'] = 5;
      self::setStatusExport($param);
      
    // проверить статус, если небыло выборки, то можно запускать
      $res = self::checkStatusExport($data['preview']);
      
      if ($res->num_rows > 0) self::displayResult($res);
      
    }
    
    
  // вставить статус выполнения экспорта
    private function setStatusExport($param){
      
      $sql = 'INSERT INTO
                `'.$this->table_export.'`
              SET 
                `date` = '.$param['date'].',
                `status` = '.$param['status'].',
                `link` = \''.$param['link'].'\'
              ON DUPLICATE KEY UPDATE 
                `link` = \''.$param['link'].'\',
                `status` = '.$param['status'].';
              ';
            
      self::quareSql($sql);
 
    }

    
  // подготовить ссылку для скачивания айла    
    private function displayNewFile($file, $error = '') {
      
      $res =  empty($error) ? ['status' => TRUE, 'message' => $file] : ['status' => FALSE, 'message' => $error];
      
      echo json_encode($res);
      
    }

  
  // главная таблица
    private function maintable() {
      return 'CREATE TABLE `'.$this->table_log.'` 
          ( `'.$this->game_time.'` INT(10) NOT NULL, 
            `'.$this->game_user_id.'` BIGINT NOT NULL , 
            `'.$this->game_device_type.'` ENUM("'.implode('","', $this->enum).'") NOT NULL,
            `'.$this->game_device_id.'` VARCHAR(64) NOT NULL
          ) ENGINE = InnoDB;';  
    }
    

  // таблица для работы с экспортом
    private function exporttable() {
      return 'CREATE TABLE `'.$this->table_export.'`
          (  `'.$this->export_date.'` INT(10) NOT NULL, 
            `'.$this->export_status.'` ENUM("'.implode('","', $this->export_enum).'") NOT NULL,
            `'.$this->export_link.'` VARCHAR(255) NOT NULL,
            `'.$this->export_update.'` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`'.$this->export_date.'`)
          ) ENGINE = InnoDB;';  
    }     
      

  // создать таблицу в БД
    private function createTable() {

      // создание главной таблицы
      $res[] = self::quareSql(self::maintable());
      
      // создание таблицы для работы с экспортом
      $res[] = self::quareSql(self::exporttable());
      
      return $res;

    }  
  
  
  // работа с БД
    private function quareSql($sql){

      // подключение к БД
      $mysqli = new mysqli($this->localhost, $this->db_user, $this->db_password, $this->db_name);

      // проверка
      if ($mysqli->connect_errno) {
        echo "Cann't connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
      }

      // выполнение запроса
      $res['sql'] = $mysqli->query($sql);

      // для проверки выполнения
      $res['error'] = $mysqli->errno;
      $res['message'] = $mysqli->error;
          
      $mysqli->close();

      return $res;
    }    
  

  // вывод сообщения
    private function displayMessage($data = ''){

      $res = (empty($data)) ? ['status' => FALSE, 'message' => 'error to save data'] : ['status' => TRUE, 'message' => 'data saved']; 

      echo (json_encode($res));

      exit;

    }
  
  
  // запуск процесса
    public function work($data){
      
      $data = self::getData($data);

      $data = self::createDataForFile($data);

      $data = self::safeDataToFile($data);
      
      return self::displayMessage($data);

    }
  
  
  // запись в БД
    public function insertIntoDb(){

      self::getFiletoTransferToMySQL();

    }
    
    
  // выгрузка данных
    public function getDataFromDb() {    
    
      $res = self::prepareToExport();
      
    }
    
  // запуск процесс получения данных на дату
    public function requestData($date) {
      
      $res = self::requestToExport($date);
      
    }
  
  // удаление данных
    public function cleareDb() {    
    
      $res = self::deleteFromDb();
      
    }
    
    
  // запуск создания таблицы и директории
    public function setup() {

      // создание директории
        $res['files'] = self::createPath();

      // создание таблицы
        $res['db'] = self::createTable();
        
        return $res;

    }
  
}