<?
class test{
  
  private $url = 'http://localhost/200persecond/data.php';
  
  // генерация параметров
  private function createOneRequest () {
        
    // игровое время события в формате Unix time
    $request['game_time'] = mt_rand(time()-140000, time()-86400);
            
    // ID игрока в Facebook (integer)
    $request['user_id'] =  100000 . mt_rand();
    
    // платформа устройства string ДОПУСКАЮТСЯ ТОЛЬКО ЗНАЧЕНИЯ: iPad, iPhone
    $request['device_type'] =  ['iPad', 'iPhone'][mt_rand(0,1)];
            
    // ID устройства (string, 64 символа)
    $request['device_id'] = md5(uniqid(mt_rand(), true)).md5(uniqid(mt_rand(), true));
   
    // дополнителььный параметр 
    $request['wrong'] = uniqid(rand(), true);
    
    return $request; 
    
  }
  
  // рандом параметров
  private function randomParametrsRequest($param) {
    
    foreach (array_rand($param, mt_rand(4,5)) as $value) {
      $array[$value] = $param[$value];
    }
    
    return($array);
    
  }

  // подготовка для запроса
  private function prepareDatatoSend($param) {
    
    return (is_array($param)) ? http_build_query($param) : $param; 
    
  }

  // отправка запроса
  private function sendPostRequest($res) {

    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL,$this->url); // URL на который посылать запрос 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_HEADER, 0); //  Результат будет содержать заголовки
    curl_setopt($ch, CURLOPT_TIMEOUT, 0); // Таймаут после 4 секунд 
    curl_setopt($ch, CURLOPT_POST, 1); // Устанавливаем метод POST
    curl_setopt($ch, CURLOPT_POSTFIELDS, self::prepareDatatoSend($res)); // посылаемые значения
    $result = curl_exec($ch);
    curl_close($ch);
    print_R($result);
  }
  
  public function createRow($row = '1'){
    
      // ограничение от случайного ввода большого кол-ва
      $max = 100000;
      $row = ($row > $max) ? $max : $row;
    
      for ($i = 0; $i < $row; $i++) {

        // генерация параметров
        $parametrs = self::createOneRequest();

        // выборка параметров
        $res = self::randomParametrsRequest($parametrs);
 
        // отправка запросов
        self::sendPostRequest($res);

      }
    
  }
  
  public function importToDb(){

    // ПЕРЕНОС ДАННЫХ В БД

    include 'index.php';

    $class = NEW run();

    print_r($class->insertIntoDb());
    
  }
  
  
}

// получение данных

$arg =[
// тип
  // create, import, export
  'type' => FILTER_SANITIZE_STRING,
  
// кол-во строк
  'row' => FILTER_SANITIZE_NUMBER_INT,

];
$ar = filter_input_array(INPUT_GET,$arg);

$r = NEW test();

if (empty($ar['type'])){
  echo '?type= create|import|export &row=10000';
  exit;
}

if ($ar['type'] == 'create'){

  // создание запросов, с указанием кол-ва
  $r->createRow($ar['row']);
  
}elseif ($ar['type'] == 'import'){
  
  // импорт в БД
  $r->importToDb();
  
}elseif ($ar['type'] == 'export'){

  // экспорт из БД
  
  
  
}
